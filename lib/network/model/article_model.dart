import 'package:json_annotation/json_annotation.dart';

part 'article_model.g.dart';

@JsonSerializable()
class Article {
  String? status;
  String? copyright;
  String? section;
  DateTime? lastUpdated;
  int? numResults;
  List<Result>? results;

  Article(
      {this.section,
      this.copyright,
      this.lastUpdated,
      this.numResults,
      this.results,
      this.status});

  factory Article.fromJson(Map<String, dynamic> json) =>
      _$ArticleFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleToJson(this);
}

@JsonSerializable()
class Result {
  String? title;
  String? abstract;
  String? url;
  String? uri;
  String? byline;
  DateTime? updatedDate;
  DateTime? createdDate;
  DateTime? publishedDate;
  String? materialTypeFacet;
  List<Multimedia>? multimedia;
  Result(
      {this.title,
      this.abstract,
      this.url,
      this.uri,
      this.byline,
      this.updatedDate,
      this.createdDate,
      this.publishedDate,
      this.materialTypeFacet,
      this.multimedia});

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);

  Map<String, dynamic> toJson() => _$ResultToJson(this);
}

@JsonSerializable()
class Multimedia {
  String? url;

  String? caption;
  String? copyright;
  Multimedia({this.url, this.caption, this.copyright});

  factory Multimedia.fromJson(Map<String, dynamic> json) =>
      _$MultimediaFromJson(json);

  Map<String, dynamic> toJson() => _$MultimediaToJson(this);
}
