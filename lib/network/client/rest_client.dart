import 'package:article_app/network/model/article_model.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../core/env.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: apiURL)
abstract class RestClient {
  factory RestClient(Dio dio, {String? baseUrl}) = _RestClient;

  @GET('/svc/topstories/v2/us.json')
  Future<Article?> getArticles(
      {@Query('api-key') String? key = 'Rp4vZd8YVke2JFPfAem9gw1ITUMpCuWQ'});
}
