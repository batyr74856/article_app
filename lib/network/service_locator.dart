import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

import 'client/rest_client.dart';
import 'core/env.dart';

enum EnvironmentType {
  development,
  production,
}

GetIt serviceLocator = GetIt.instance;

void serviceLocatorSetup(EnvironmentType envType) {
  serviceLocator
    ..registerFactory<RestClient>(() {
      final dio = Dio();

      return RestClient(dio, baseUrl: apiURL);
    });
}
