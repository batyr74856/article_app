import 'dart:async';

import 'package:article_app/home/ui/home.dart';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Timer(
        const Duration(seconds: 2),
        () => Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                // ignore: unnecessary_null_comparison
                builder: (context) => HomePage()),
            ModalRoute.withName("/FirstScreen")));
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.white,
      appBar: null,
      body: Center(
          child: Image(
        image: AssetImage("assets/icons/app_logo.png"),
        width: 100,
        height: 100,
      )),
    );
  }
}
