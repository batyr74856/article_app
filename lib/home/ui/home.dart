import 'package:article_app/home/widgets/article_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/article_bloc.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  String? name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
          child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
            child: TextFormField(
              cursorColor: Colors.black,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: Colors.black),
              textAlign: TextAlign.left,
              onChanged: (value) {
                name = value;
              },
              decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(vertical: 14, horizontal: 26),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.greenAccent),
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  hintText: 'Поиск...',
                  hintStyle: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Colors.grey),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide.none),
                  filled: true,
                  fillColor: Color.fromRGBO(
                    245,
                    245,
                    245,
                    1,
                  )),
            ),
          ),
          BlocBuilder<ArticleBloc, ArticleState>(
            builder: (context, state) {
              if (state is ArticleInitial) {
                return SizedBox();
              } else if (state is ArticleLoading) {
                return LinearProgressIndicator();
              } else if (state is ArticleLoaded) {
                return Column(
                  children: [
                    ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      physics: ScrollPhysics(),
                      itemCount: state.article!.results!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ArticleDetails(
                                          image:
                                              '${state.article!.results![index].multimedia![0].url}',
                                          title:
                                              '${state.article!.results![index].title}',
                                          abstract:
                                              '${state.article!.results![index].abstract}',
                                          author:
                                              '${state.article!.results![index].byline}',
                                          section: '${state.article!.section}',
                                          url:
                                              '${state.article!.results![index].url}',
                                        )));
                          },
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(
                                  '${state.article!.results![index].title}',
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Montserrat',
                                    color: Color.fromRGBO(
                                      42,
                                      43,
                                      55,
                                      1,
                                    ),
                                  ),
                                ),
                                subtitle: Text(
                                  '${state.article!.results![index].byline}',
                                  style: TextStyle(
                                      fontSize: 11,
                                      fontWeight: FontWeight.w500,
                                      fontFamily: 'Montserrat',
                                      color: Colors.grey),
                                ),
                              ),
                              Image.network(
                                '${state.article!.results![index].multimedia![0].url}',
                              ),
                              Divider(),
                            ],
                          ),
                        );
                      },
                    ),
                    Center(
                      child: Text(
                        '${state.article!.copyright}',
                        style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            fontFamily: 'Montserrat',
                            color: Colors.grey),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                );
              } else {
                return SizedBox();
              }
            },
          ),
        ],
      )),
    );
  }
}
