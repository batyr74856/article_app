part of 'article_bloc.dart';

abstract class ArticleEvent extends Equatable {}

class FetchArticles extends ArticleEvent {
  @override
  List<Object> get props => [];
}
