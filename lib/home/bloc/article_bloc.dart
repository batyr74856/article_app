import 'dart:async';

import 'package:article_app/home/repositories/article_provider.dart';
import 'package:article_app/network/model/article_model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'article_event.dart';
part 'article_state.dart';

class ArticleBloc extends Bloc<ArticleEvent, ArticleState> {
  ArticleProvider articleProvider = ArticleProvider();
  ArticleBloc({required this.articleProvider}) : super(ArticleInitial()) {
    on<FetchArticles>(fetchArticle);
  }

  FutureOr<void> fetchArticle(
      FetchArticles event, Emitter<ArticleState> emit) async {
    try {
      Article? getData = await articleProvider.getArticleData();
      if (getData == null) {
        return;
      }

      emit(ArticleLoaded(article: getData));
    } catch (e) {
      throw (e);
    }
  }
}
