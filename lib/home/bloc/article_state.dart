part of 'article_bloc.dart';

abstract class ArticleState extends Equatable {
  ArticleState();
}

class ArticleInitial extends ArticleState {
  @override
  List<Object> get props => [];
}

class ArticleLoading extends ArticleState {
  @override
  List<Object> get props => [];
}

class ArticleLoaded extends ArticleState {
  final Article? article;

  ArticleLoaded({required this.article});
  @override
  List<Object> get props => [];
}
