import 'package:article_app/network/model/article_model.dart';

import '../../network/client/rest_client.dart';
import '../../network/service_locator.dart';

class ArticleProvider {
  Future<Article?> getArticleData() async {
    try {
      var response = await serviceLocator<RestClient>().getArticles();
      return response;
    } catch (e) {
      throw (e);
    }
  }
}
