import 'package:article_app/home/bloc/article_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'const/splash_screen.dart';
import 'home/repositories/article_provider.dart';
import 'network/service_locator.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  serviceLocatorSetup(EnvironmentType.development);
  runApp(MyApp());
}

ArticleProvider articleProvider = ArticleProvider();

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ArticleBloc>(
          create: ((context) => ArticleBloc(articleProvider: articleProvider)
            ..add(FetchArticles())),
        ),
      ],
      child: MaterialApp(
        title: 'Article App',
        debugShowCheckedModeBanner: false,
        home: SplashScreen(),
      ),
    );
  }
}
